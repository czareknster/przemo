import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Guitar } from './../models/guitar.model';

@Injectable()
export class GuitarService {
  constructor(private http: HttpClient) {}

  getGitars() {
    return this.http.get<Guitar[]>(`/api/v1/guitars`);
  }
}
