import { async, TestBed } from '@angular/core/testing';
import { GuitarModule } from './guitar.module';

describe('GuitarModule', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [GuitarModule],
    }).compileComponents();
  }));

  it('should create', () => {
    expect(GuitarModule).toBeDefined();
  });
});
