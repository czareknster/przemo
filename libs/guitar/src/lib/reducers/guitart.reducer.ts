import { createReducer, on } from '@ngrx/store';
import { EntityState, EntityAdapter, createEntityAdapter } from '@ngrx/entity';

import { Guitar } from './../models/guitar.model';

import * as guitarActions from './../actions/guitar.actions';

export interface State extends EntityState<Guitar> {
  loading: boolean;
  selectedId: string;
}

export const adapter: EntityAdapter<Guitar> = createEntityAdapter<Guitar>({
  selectId: (guitar: Guitar) => guitar.guitarId,
});

const initialState: State = adapter.getInitialState({
  loading: false,
  selectedId: null,
});

export const reduser = createReducer(
  initialState,
  on(guitarActions.load, (state) => ({ ...state, loading: true })),
  on(guitarActions.loadSuccess, (state, { items }) =>
    adapter.setAll(items, { ...state, loading: false })
  ),
  on(guitarActions.selectGuitar, (state, { id }) => ({
    ...state,
    selectedId: id,
  }))
);
