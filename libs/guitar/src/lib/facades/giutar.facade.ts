import { Injectable } from '@angular/core';
import { Store, select } from '@ngrx/store';

import * as guitarActions from './../actions/guitar.actions';

import {
  State,
  getAllGuitars,
  getGiutarsLoading,
  getSelectedGiutar,
  getGiutarsSelectedId,
} from './../reducers';

@Injectable()
export class GuitarFacade {
  constructor(private readonly store: Store<State>) {}

  readonly getAllGuitars$ = this.store.pipe(select(getAllGuitars));
  readonly getAllGuitarsLoading$ = this.store.pipe(select(getGiutarsLoading));
  readonly getSelectedGiutar$ = this.store.pipe(select(getSelectedGiutar));
  readonly getGiutarsSelectedId$ = this.store.pipe(
    select(getGiutarsSelectedId)
  );

  load() {
    this.store.dispatch(guitarActions.load());
  }

  select(id: string) {
    this.store.dispatch(guitarActions.selectGuitar({ id }));
  }
}
