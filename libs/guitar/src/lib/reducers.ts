import * as R from 'ramda';
import {
  createFeatureSelector,
  createSelector,
  combineReducers,
  Action,
} from '@ngrx/store';

import * as fromGuitar from './reducers/guitart.reducer';

export interface GuitarsState {
  fromGuitars: fromGuitar.State;
}

export interface State {
  guitars: GuitarsState;
}

export function reducers(state: GuitarsState, action: Action) {
  return combineReducers({
    fromGuitars: fromGuitar.reduser,
  })(state, action);
}

const getFeatureState = createFeatureSelector<State, GuitarsState>('guitars');

export const getGiutarsState = createSelector(
  getFeatureState,
  (state) => state.fromGuitars
);

export const {
  selectAll: getAllGuitars,
  selectIds: getGuitarsIds,
  selectEntities: getGuitarsEntities,
} = fromGuitar.adapter.getSelectors(getGiutarsState);

export const getGiutarsLoading = createSelector(
  getGiutarsState,
  (state) => state.loading
);

export const getGiutarsSelectedId = createSelector(
  getGiutarsState,
  (state) => state.selectedId
);

export const getSelectedGiutar = createSelector(
  getGuitarsEntities,
  getGiutarsSelectedId,
  (entities, selectedId) => entities[selectedId]
);
