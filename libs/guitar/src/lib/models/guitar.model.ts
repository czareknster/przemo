export interface Guitar {
  guitarId: string;
  name: string;
  creationDate: string;
  modificationDate: string;
  price: number;
}
