import { Injectable } from '@angular/core';
import { Actions, ofType, Effect } from '@ngrx/effects';
import { of } from 'rxjs';
import { switchMap, map, catchError } from 'rxjs/operators';

import * as GuitarActions from './../actions/guitar.actions';

import { GuitarService } from './../services/guitar.servis';

import { Guitar } from './../models/guitar.model';

@Injectable()
export class GuitarEffects {
  constructor(
    private readonly actions$: Actions,
    private readonly guitarService: GuitarService
  ) {}

  @Effect()
  load$ = this.actions$.pipe(
    ofType(GuitarActions.load),
    switchMap(() =>
      this.guitarService.getGitars().pipe(
        map((items: Guitar[]) => GuitarActions.loadSuccess({ items })),
        catchError((error) => of(GuitarActions.loadError(error)))
      )
    )
  );
}
