import {
  Component,
  Input,
  ChangeDetectionStrategy,
  Output,
  EventEmitter,
} from '@angular/core';

import { Guitar } from './../../models/guitar.model';

@Component({
  selector: 'przemo-guitar-details',
  templateUrl: './guitar-details.component.html',
  styleUrls: ['./guitar-details.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class GuitarDetailsComponent {
  @Input() guitars: Guitar[];

  @Output() loadGuitars: EventEmitter<any> = new EventEmitter();

  loadGuitar() {
    this.loadGuitars.emit();
  }
}
