import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GuitarHomeComponent } from './guitar-home.component';

describe('GuitarHomeComponent', () => {
  let component: GuitarHomeComponent;
  let fixture: ComponentFixture<GuitarHomeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GuitarHomeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GuitarHomeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
