import { Component, ChangeDetectionStrategy } from '@angular/core';

import { GuitarFacade } from './../../facades/giutar.facade';

@Component({
  selector: 'przemo-guitar-home',
  templateUrl: './guitar-home.component.html',
  styleUrls: ['./guitar-home.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class GuitarHomeComponent {
  constructor(private guitarFacade: GuitarFacade) {}

  readonly allGuitars$ = this.guitarFacade.getAllGuitars$;

  loadGuitar() {
    this.guitarFacade.load();
  }
}
