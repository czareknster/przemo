import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';

// Store
import { StoreModule } from '@ngrx/store';
import { reducers } from './reducers';
import { EffectsModule } from '@ngrx/effects';
import { effects } from './effects';

// Facades
import { GuitarFacade } from './facades/giutar.facade';

// Services

import { GuitarService } from './services/guitar.servis';

import { GuitarHomeComponent } from './components/guitar-home/guitar-home.component';
import { GuitarDetailsComponent } from './components/guitar-details/guitar-details.component';

@NgModule({
  imports: [
    CommonModule,
    HttpClientModule,
    RouterModule.forChild([
      {
        path: '',
        component: GuitarHomeComponent,
      },
    ]),
    StoreModule.forFeature('guitars', reducers),
    EffectsModule.forFeature(effects),
  ],
  providers: [GuitarFacade, GuitarService],
  declarations: [GuitarHomeComponent, GuitarDetailsComponent],
})
export class GuitarModule {}
