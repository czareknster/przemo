import { createAction, props } from '@ngrx/store';
import { Guitar } from './../models/guitar.model';

export const load = createAction('[Guitar] - Load');

export const loadSuccess = createAction(
  '[Guitar] - Load success',
  props<{ items: Guitar[] }>()
);

export const loadError = createAction(
  '[Guitar] - Load Error',
  props<{ error: any }>()
);

export const selectGuitar = createAction(
  '[Guitar] - Select',
  props<{ id: string }>()
);
