import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { REDUCER_TOKEN } from './recucers/app.reducer';

import { GuitarFacade } from '@przemo/guitar';

import { AppComponent } from './app.component';
import { HomeComponent } from './components/home/home.component';

const routes: Routes = [
  { path: 'home', component: HomeComponent },
  {
    path: 'przemo',
    loadChildren: () => import('@przemo/guitar').then((m) => m.GuitarModule),
  },
  {
    path: '',
    pathMatch: 'full',
    redirectTo: 'home',
  },
  {
    path: '**',
    redirectTo: 'home',
  },
];

@NgModule({
  declarations: [AppComponent, HomeComponent],
  imports: [
    BrowserModule,
    RouterModule.forRoot(routes),
    StoreModule.forRoot(REDUCER_TOKEN),
    EffectsModule.forRoot([]),
    StoreDevtoolsModule.instrument({
      name: 'Przemo-App',
    }),
  ],
  providers: [GuitarFacade],
  bootstrap: [AppComponent],
})
export class AppModule {}
