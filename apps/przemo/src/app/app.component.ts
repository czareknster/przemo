import { Component } from '@angular/core';

@Component({
  selector: 'for-przemo-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent {
  title = 'przemo';
}
