import { createReducer, on } from '@ngrx/store';
import * as ViewActions from '../actions/view.actions';

export interface State {
  backButton: boolean;
  overviewSidebar: boolean;
}

export const initialState = {
  backButton: false,
  overviewSidebar: false,
};

export const reducer = createReducer(
  initialState,
  on(ViewActions.openOverviewSidebar, (state) => ({
    ...state,
    overviewSidebar: true,
  }))
);
