import {
  ActionReducerMap,
  createFeatureSelector,
  createSelector,
} from '@ngrx/store';

import * as fromViews from './views.reducer';
import { InjectionToken } from '@angular/core';

export interface AppState {
  views: fromViews.State;
}

export const REDUCER_TOKEN = new InjectionToken<ActionReducerMap<AppState>>(
  'Registered Reducers',
  {
    factory: () => {
      return {
        views: fromViews.reducer,
      };
    },
  }
);

const selectViewState = createFeatureSelector<AppState, fromViews.State>(
  'views'
);

// Views
export const selectOverviewSidebarState = createSelector(
  selectViewState,
  (state) => state.overviewSidebar
);
