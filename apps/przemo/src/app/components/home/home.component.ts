import { Component, ChangeDetectionStrategy } from '@angular/core';

import { GuitarFacade } from '@przemo/guitar';

@Component({
  selector: 'for-przemo-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class HomeComponent {
  constructor(private guitarFacade: GuitarFacade) {}

  readonly guitarSelectedId$ = this.guitarFacade.getGiutarsSelectedId$;

  setSelectedId() {
    this.guitarFacade.select('0fad19c2-d6e2-45c1-bwer3-b8ca543821dc');
  }
}
