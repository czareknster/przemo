import { createAction } from '@ngrx/store';
export const openOverviewSidebar = createAction('[App] Open overview sidebar');
