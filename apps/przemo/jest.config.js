module.exports = {
  name: 'przemo',
  preset: '../../jest.config.js',
  coverageDirectory: '../../coverage/apps/przemo',
  snapshotSerializers: [
    'jest-preset-angular/build/AngularNoNgAttributesSnapshotSerializer.js',
    'jest-preset-angular/build/AngularSnapshotSerializer.js',
    'jest-preset-angular/build/HTMLCommentSerializer.js',
  ],
};
